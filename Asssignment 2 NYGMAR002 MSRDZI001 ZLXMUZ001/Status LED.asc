Version 4
SHEET 1 880 680
WIRE 16 160 -80 160
WIRE 144 160 144 112
WIRE 144 160 96 160
WIRE 176 160 144 160
WIRE 320 160 240 160
WIRE 320 208 320 160
WIRE -208 256 -208 208
WIRE -208 368 -208 336
FLAG 320 208 0
FLAG -80 160 Vin
IOPIN -80 160 In
FLAG -208 368 0
FLAG -208 208 Vin
IOPIN -208 208 Out
FLAG 144 112 VLED
IOPIN 144 112 Out
SYMBOL LED 176 176 R270
WINDOW 0 72 32 VTop 2
WINDOW 3 0 32 VBottom 2
SYMATTR InstName D1
SYMATTR Value NSPW500BS
SYMBOL res 112 144 R90
WINDOW 0 0 56 VBottom 2
WINDOW 3 32 56 VTop 2
SYMATTR InstName R1
SYMATTR Value 100
SYMBOL voltage -208 240 R0
WINDOW 123 0 0 Left 0
WINDOW 39 0 0 Left 0
SYMATTR InstName V1
SYMATTR Value 3.3
TEXT -280 392 Left 2 !.tran 1
