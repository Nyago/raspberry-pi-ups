(part "RJK0305DPB-02_J0"
    (packageRef "RJK0305DPB02J0")
    (interface
        (port "1" (symbPinId 1) (portName "S_1") (portType INOUT))
        (port "2" (symbPinId 2) (portName "S_2") (portType INOUT))
        (port "3" (symbPinId 3) (portName "S_3") (portType INOUT))
        (port "4" (symbPinId 4) (portName "G") (portType INOUT))
        (port "5" (symbPinId 5) (portName "D_1") (portType INOUT))
        (port "6" (symbPinId 6) (portName "D_2") (portType INOUT))
    )
    (partClass UNDEF)
    (useInSchema Y)
    (useInLayout Y)
    (inPartsList Y)
    (partType NORMAL)
    (placeRestriction FREE)
    (property "compKind" "221")
    (property "Manufacturer_Name" "Renesas Electronics")
    (property "Manufacturer_Part_Number" "RJK0305DPB-02#J0")
    (property "Mouser_Part_Number" "968-RJK0305DPB-02#J0")
    (property "Mouser_Price/Stock" "https://www.mouser.co.uk/ProductDetail/Renesas-Electronics/RJK0305DPB-02J0/?qs=ZVKuL1Ob8AsE1GTfId2KKQ%3D%3D")
    (property "Arrow_Part_Number" "")
    (property "Arrow_Price/Stock" "")
    (property "Description" "MOSFET N-CH 30V 30A LFPAK")
    (property "Datasheet_Link" "https://www.renesas.com/en-us/doc/products/transistor/003/r07ds1245ej0901_rjk0305dpb.pdf")
    (property "symbolName1" "RJK0305DPB-02_J0")
)
