*PADS-LIBRARY-PART-TYPES-V9*

CMDSH2-3_TR SOD2512X110N I DIO 9 1 0 0 0
TIMESTAMP 2021.06.04.02.03.56
"Manufacturer_Name" Central Semiconductor
"Manufacturer_Part_Number" CMDSH2-3 TR
"Mouser Part Number" 610-CMDSH2-3
"Mouser Price/Stock" https://www.mouser.com/Search/Refine.aspx?Keyword=610-CMDSH2-3
"Arrow Part Number" 
"Arrow Price/Stock" 
"Description" Schottky Diodes & Rectifiers 30V Schottky
"Datasheet Link" https://my.centralsemi.com/get_document.php?cmp=1&mergetype=pd&mergepath=pd&pdf_id=CMDSH2-3.PDF
"Geometry.Height" 1.1mm
GATE 1 2 0
CMDSH2-3_TR
1 0 U K
2 0 U A

*END*
*REMARK* SamacSys ECAD Model
845491/519398/2.49/2/3/Diode
