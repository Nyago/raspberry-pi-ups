EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 4
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text GLabel 3350 3150 0    50   Input ~ 0
GRD
$Comp
L RJK0305DPB-02#J0:RJK0305DPB-02#J0 Q2
U 1 1 60BA20CE
P 6350 2400
F 0 "Q2" H 6850 2665 50  0000 C CNN
F 1 "RJK0305DPB-02#J0" H 6850 2574 50  0000 C CNN
F 2 "RJK0305DPB-02#J0:RJK0305DPB02J0" H 7200 2500 50  0001 L CNN
F 3 "https://www.renesas.com/en-us/doc/products/transistor/003/r07ds1245ej0901_rjk0305dpb.pdf" H 7200 2400 50  0001 L CNN
F 4 "MOSFET N-CH 30V 30A LFPAK" H 7200 2300 50  0001 L CNN "Description"
F 5 "1.1" H 7200 2200 50  0001 L CNN "Height"
F 6 "Renesas Electronics" H 7200 2100 50  0001 L CNN "Manufacturer_Name"
F 7 "RJK0305DPB-02#J0" H 7200 2000 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "968-RJK0305DPB-02#J0" H 7200 1900 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.co.uk/ProductDetail/Renesas-Electronics/RJK0305DPB-02J0/?qs=ZVKuL1Ob8AsE1GTfId2KKQ%3D%3D" H 7200 1800 50  0001 L CNN "Mouser Price/Stock"
F 10 "" H 7200 1700 50  0001 L CNN "Arrow Part Number"
F 11 "" H 7200 1600 50  0001 L CNN "Arrow Price/Stock"
	1    6350 2400
	-1   0    0    1   
$EndComp
$Comp
L RJK0301DPB-02#J0:RJK0301DPB-02#J0 Q1
U 1 1 60BA2D70
P 5650 3200
F 0 "Q1" H 6100 3465 50  0000 C CNN
F 1 "RJK0301DPB-02#J0" H 6100 3374 50  0000 C CNN
F 2 "RJK0301DPB-02#J0:LFPAK" H 6400 3300 50  0001 L CNN
F 3 "https://www.renesas.com/en-us/doc/products/transistor/003/r07ds1244ej0901_rjk0301dpb.pdf" H 6400 3200 50  0001 L CNN
F 4 "MOSFET POWER TRANSISTOR LV MOS, 30V, LFPAK" H 6400 3100 50  0001 L CNN "Description"
F 5 "" H 6400 3000 50  0001 L CNN "Height"
F 6 "Renesas Electronics" H 6400 2900 50  0001 L CNN "Manufacturer_Name"
F 7 "RJK0301DPB-02#J0" H 6400 2800 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "968-RJK0301DPB-02#J0" H 6400 2700 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.com/Search/Refine.aspx?Keyword=968-RJK0301DPB-02%23J0" H 6400 2600 50  0001 L CNN "Mouser Price/Stock"
F 10 "RJK0301DPB-02#J0" H 6400 2500 50  0001 L CNN "Arrow Part Number"
F 11 "https://www.arrow.com/en/products/rjk0301dpb-02j0/renesas-electronics" H 6400 2400 50  0001 L CNN "Arrow Price/Stock"
	1    5650 3200
	1    0    0    -1  
$EndComp
$Comp
L kicad-files-rescue:CMDSH2-3_TR-CMDSH2-3_TR D?
U 1 1 60BADD05
P 4500 2900
AR Path="/60BADD05" Ref="D?"  Part="1" 
AR Path="/60B90983/60BADD05" Ref="D7"  Part="1" 
F 0 "D7" H 4800 3167 50  0000 C CNN
F 1 "CMDSH2-3_TR" H 4800 3076 50  0000 C CNN
F 2 "CMDSH2-3_TR:SOD2512X110N" H 4950 2900 50  0001 L CNN
F 3 "https://my.centralsemi.com/get_document.php?cmp=1&mergetype=pd&mergepath=pd&pdf_id=CMDSH2-3.PDF" H 4950 2800 50  0001 L CNN
F 4 "Schottky Diodes & Rectifiers 30V Schottky" H 4950 2700 50  0001 L CNN "Description"
F 5 "1.1" H 4950 2600 50  0001 L CNN "Height"
F 6 "Central Semiconductor" H 4950 2500 50  0001 L CNN "Manufacturer_Name"
F 7 "CMDSH2-3 TR" H 4950 2400 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "610-CMDSH2-3" H 4950 2300 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.com/Search/Refine.aspx?Keyword=610-CMDSH2-3" H 4950 2200 50  0001 L CNN "Mouser Price/Stock"
F 10 "" H 4950 2100 50  0001 L CNN "Arrow Part Number"
F 11 "" H 4950 2000 50  0001 L CNN "Arrow Price/Stock"
	1    4500 2900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4050 2150 4600 2150
Wire Wire Line
	2650 2450 2550 2450
Wire Wire Line
	2350 2350 2350 3550
Wire Wire Line
	4600 2150 4600 900 
Wire Wire Line
	4600 900  4700 900 
Connection ~ 4600 2150
Wire Wire Line
	5150 3550 5150 2400
Wire Wire Line
	5150 2400 5350 2400
Wire Wire Line
	2350 3550 5150 3550
Wire Wire Line
	5350 2300 5100 2300
Wire Wire Line
	5100 2300 5100 2150
Wire Wire Line
	4600 2150 5100 2150
Wire Wire Line
	6350 2200 6350 2300
Connection ~ 6350 2300
Wire Wire Line
	6350 2300 6350 2400
Wire Wire Line
	6350 2300 6550 2300
Wire Wire Line
	4050 2250 4500 2250
Wire Wire Line
	4500 2250 4500 2300
Wire Wire Line
	2650 2350 2350 2350
Wire Wire Line
	2650 2250 2450 2250
Wire Wire Line
	2450 2250 2450 3000
Wire Wire Line
	2450 3000 2700 3000
Wire Wire Line
	2700 3000 2700 3250
Wire Wire Line
	4500 3250 4500 2900
Wire Wire Line
	2700 3250 4500 3250
Wire Wire Line
	2550 2450 2550 4450
Wire Wire Line
	2550 4450 4500 4450
Connection ~ 4500 3250
Connection ~ 4500 4450
Wire Wire Line
	4500 4450 5550 4450
$Comp
L pspice:INDUCTOR L1
U 1 1 60BBA057
P 5950 4450
F 0 "L1" H 5950 4665 50  0000 C CNN
F 1 "0.000006" H 5950 4574 50  0000 C CNN
F 2 "Inductor_SMD:L_6.3x6.3_H3" H 5950 4450 50  0001 C CNN
F 3 "~" H 5950 4450 50  0001 C CNN
	1    5950 4450
	1    0    0    -1  
$EndComp
$Comp
L Device:R R8
U 1 1 60BBAB0E
P 6650 4450
F 0 "R8" V 6857 4450 50  0000 C CNN
F 1 "0.005" V 6766 4450 50  0000 C CNN
F 2 "Resistor_SMD:R_0201_0603Metric" V 6580 4450 50  0001 C CNN
F 3 "~" H 6650 4450 50  0001 C CNN
	1    6650 4450
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C5
U 1 1 60BBB193
P 7250 4850
F 0 "C5" H 7365 4896 50  0000 L CNN
F 1 "0.00066" H 7365 4805 50  0000 L CNN
F 2 "Capacitor_SMD:C_0201_0603Metric" H 7288 4700 50  0001 C CNN
F 3 "~" H 7250 4850 50  0001 C CNN
	1    7250 4850
	1    0    0    -1  
$EndComp
Wire Wire Line
	7250 4450 7250 4700
Wire Wire Line
	7250 5000 7250 5800
Wire Wire Line
	4150 1950 4050 1950
Wire Wire Line
	4050 2050 4350 2050
Wire Wire Line
	6200 4450 6400 4450
$Comp
L LTC3854EMSE#PBF:LTC3854EMSE#PBF IC1
U 1 1 60B9C246
P 2650 1950
F 0 "IC1" H 3350 2215 50  0000 C CNN
F 1 "LTC3854EMSE#PBF" H 3350 2124 50  0000 C CNN
F 2 "LTC3854EMSE#PBF:SOP65P490X110-13N" H 3900 2050 50  0001 L CNN
F 3 "http://www.linear.com/docs/27999" H 3900 1950 50  0001 L CNN
F 4 "LTC3854,DC-DC Controller,StepDown MSOP12 Linear Technology LTC3854EMSE#PBF, DC-DC Controller, 40mA, 38 V, Step Down, 440 kHz, 12-Pin, MSOP" H 3900 1850 50  0001 L CNN "Description"
F 5 "1.1" H 3900 1750 50  0001 L CNN "Height"
F 6 "Linear Technology" H 3900 1650 50  0001 L CNN "Manufacturer_Name"
F 7 "LTC3854EMSE#PBF" H 3900 1550 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "584-LTC3854EMSE#PBF" H 3900 1450 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.co.uk/ProductDetail/Analog-Devices-Linear-Technology/LTC3854EMSEPBF?qs=hVkxg5c3xu8iwAm47HpOjg%3D%3D" H 3900 1350 50  0001 L CNN "Mouser Price/Stock"
F 10 "" H 3900 1250 50  0001 L CNN "Arrow Part Number"
F 11 "" H 3900 1150 50  0001 L CNN "Arrow Price/Stock"
	1    2650 1950
	1    0    0    -1  
$EndComp
Wire Wire Line
	2150 1600 2150 4800
Wire Wire Line
	2150 4800 6400 4800
Wire Wire Line
	6400 4800 6400 4450
Connection ~ 6400 4450
Wire Wire Line
	6400 4450 6500 4450
Wire Wire Line
	6800 4450 6950 4450
Wire Wire Line
	6950 4450 6950 5150
Wire Wire Line
	6950 5150 1700 5150
Wire Wire Line
	1700 5150 1700 2950
Wire Wire Line
	1700 1950 2650 1950
Connection ~ 6950 4450
Wire Wire Line
	6950 4450 7250 4450
Wire Wire Line
	4150 1600 2150 1600
Wire Wire Line
	4150 1950 4150 1600
$Comp
L Device:R R7
U 1 1 60BDC11C
P 1700 2400
F 0 "R7" H 1770 2446 50  0000 L CNN
F 1 "42200" H 1770 2355 50  0000 L CNN
F 2 "Resistor_SMD:R_0201_0603Metric" V 1630 2400 50  0001 C CNN
F 3 "~" H 1700 2400 50  0001 C CNN
	1    1700 2400
	1    0    0    -1  
$EndComp
Wire Wire Line
	1700 2250 1700 1950
$Comp
L Device:R R6
U 1 1 60BDC695
P 1700 1700
F 0 "R6" H 1770 1746 50  0000 L CNN
F 1 "8060" H 1770 1655 50  0000 L CNN
F 2 "Resistor_SMD:R_0201_0603Metric" V 1630 1700 50  0001 C CNN
F 3 "~" H 1700 1700 50  0001 C CNN
	1    1700 1700
	1    0    0    -1  
$EndComp
Wire Wire Line
	1700 1550 1700 1300
Wire Wire Line
	1700 1850 1700 1950
Connection ~ 1700 1950
Wire Wire Line
	4350 2050 4350 800 
Wire Wire Line
	4350 800  1200 800 
Wire Wire Line
	1200 800  1200 2950
Wire Wire Line
	1200 2950 1700 2950
Connection ~ 1700 2950
Wire Wire Line
	1700 2950 1700 2550
$Comp
L Device:C C2
U 1 1 60BDFDD7
P 2350 2050
F 0 "C2" V 2098 2050 50  0000 C CNN
F 1 "0.000000002" V 2189 2050 50  0000 C CNN
F 2 "Capacitor_SMD:C_0201_0603Metric" H 2388 1900 50  0001 C CNN
F 3 "~" H 2350 2050 50  0001 C CNN
	1    2350 2050
	0    1    1    0   
$EndComp
$Comp
L Device:C C1
U 1 1 60BE297F
P 1950 2150
F 0 "C1" V 1698 2150 50  0000 C CNN
F 1 "0.000000002" V 1789 2150 50  0000 C CNN
F 2 "Capacitor_SMD:C_0201_0603Metric" H 1988 2000 50  0001 C CNN
F 3 "~" H 1950 2150 50  0001 C CNN
	1    1950 2150
	0    1    1    0   
$EndComp
Wire Wire Line
	2650 2050 2500 2050
Wire Wire Line
	2200 2050 2050 2050
Wire Wire Line
	2050 2050 2050 1350
Wire Wire Line
	2650 2150 2100 2150
Wire Wire Line
	1800 2150 1450 2150
Wire Wire Line
	1450 2150 1450 1300
Wire Wire Line
	4050 2350 4150 2350
Wire Wire Line
	4150 2350 4150 3100
Wire Wire Line
	4150 3100 5000 3100
Wire Wire Line
	5000 3100 5000 3450
Wire Wire Line
	5000 3450 5450 3450
Wire Wire Line
	5450 3450 5450 3500
Wire Wire Line
	5450 3500 5650 3500
Wire Wire Line
	5650 3400 5650 3300
Wire Wire Line
	5650 3200 5650 3300
Connection ~ 5650 3300
Wire Wire Line
	5650 3300 5500 3300
Wire Wire Line
	4050 2450 4050 2700
Wire Wire Line
	5500 3050 5500 3300
Wire Wire Line
	6550 3200 6550 3850
Wire Wire Line
	6550 3850 5550 3850
Wire Wire Line
	5550 3850 5550 4450
Connection ~ 5550 4450
Wire Wire Line
	5550 4450 5700 4450
Wire Wire Line
	6550 3200 6550 2300
Connection ~ 6550 3200
Wire Wire Line
	4500 2250 4900 2250
Wire Wire Line
	4900 2250 4900 1550
Wire Wire Line
	4900 1550 5150 1550
Connection ~ 4500 2250
$Comp
L Device:C C4
U 1 1 60BFF237
P 5300 1550
F 0 "C4" V 5552 1550 50  0000 C CNN
F 1 "0.0000047" V 5461 1550 50  0000 C CNN
F 2 "Capacitor_SMD:C_0201_0603Metric" H 5338 1400 50  0001 C CNN
F 3 "~" H 5300 1550 50  0001 C CNN
	1    5300 1550
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C3
U 1 1 60C00DB1
P 4500 3900
F 0 "C3" H 4615 3946 50  0000 L CNN
F 1 "0.0000001" H 4615 3855 50  0000 L CNN
F 2 "Capacitor_SMD:C_0201_0603Metric" H 4538 3750 50  0001 C CNN
F 3 "~" H 4500 3900 50  0001 C CNN
	1    4500 3900
	1    0    0    -1  
$EndComp
Wire Wire Line
	4500 3250 4500 3750
Wire Wire Line
	4500 4050 4500 4450
Connection ~ 7250 4450
Text GLabel 7250 5800 0    50   Input ~ 0
GRD
Text GLabel 1450 1300 0    50   Input ~ 0
GRD
Text GLabel 1700 1300 0    50   Input ~ 0
GRD
Text GLabel 2050 1350 0    50   Input ~ 0
GRD
Text GLabel 5450 1550 2    50   Input ~ 0
GRD
Text GLabel 4050 2700 0    50   Input ~ 0
GRD
Text GLabel 5500 3050 1    50   Input ~ 0
GRD
Text HLabel 4700 900  1    50   Input ~ 0
Vin
Text HLabel 8450 4450 2    50   Output ~ 0
Vout_5V
NoConn ~ 5350 2200
Wire Wire Line
	7250 4450 8450 4450
$EndSCHEMATC
